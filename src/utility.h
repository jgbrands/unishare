/* utility.h
 *
 * Copyright 2019 Jesse Gerard Brands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtkmm/builder.h>

template<typename T, typename... Args>
Glib::RefPtr<T>
get_derived_widget(const Glib::RefPtr<Gtk::Builder>& builder, const std::string& name, Args&& ... args)
{
    T* ptr = nullptr;
    builder->get_widget_derived<T>(name, ptr, args...);
    return Glib::RefPtr<T>(ptr);
}
