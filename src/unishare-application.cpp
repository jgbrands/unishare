#include "utility.h"
#include "unishare-application.h"

UnishareApplication::UnishareApplication()
        : Gtk::Application("nl.jgbrands.Unishare",
                           Gio::APPLICATION_FLAGS_NONE)
{

}

void UnishareApplication::on_startup()
{
    Application::on_startup();

    auto builder = Gtk::Builder::create_from_resource("/nl/jgbrands/Unishare/unishare-window.ui");
    applicationWindow = get_derived_widget<UnishareWindow>(builder, "ApplicationWindow");


}

void UnishareApplication::on_activate()
{
    applicationWindow->present();
    add_window(*applicationWindow.get());
}


