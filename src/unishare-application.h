#include <memory>

#include <gtkmm/application.h>

#include "unishare-window.h"

class UnishareApplication : public Gtk::Application {
  protected:
    Glib::RefPtr<UnishareWindow> applicationWindow;

    void on_startup() override;
    void on_activate() override;

  public:
    UnishareApplication();
};
